#pragma warning disable CS8618
using System.Runtime.InteropServices;
using ComponentTests.ExternalEnvironment.Containers;
using ComponentTests.Helpers.Clients.RabbitMq;
using Docker.DotNet;
using Docker.DotNet.Models;
using MbDotNet;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;

namespace ComponentTests.ExternalEnvironment;

/// <summary>
/// Класс для работы с внешними зависимостями
/// </summary>
public static class ExtEnvironment
{
    /// <summary>
    /// Postgres контейнер
    /// </summary>
    public static PostgresContainer PostgresContainer { get; set; }    
    
    /// <summary>
    /// MountebankContainer контейнер
    /// </summary>
    public static MountebankContainer MountebankContainer { get; set; }
    
    /// <summary>
    /// RabbitMQ контейнер
    /// </summary>
    public static RabbitmqContainer RabbitmqContainer { get; set; }

    /// <summary>
    /// Тестовый сервер приложения
    /// </summary>
    public static TestServer TestServer { get; set;}
    
    /// <summary>
    /// Клиент для маунтбанка
    /// </summary>
    public static MountebankClient MountebankClient { get; private set; } = new(new Uri("http://localhost:2525"));
    
    /// <summary>
    /// Клиент для RabbitMq
    /// </summary>
    public static RabbitMqClient RabbitMqClient { get; private set; } = new();

    /// <summary>
    /// ctor
    /// </summary>
    public static async Task Start()
    {
        var dockerClient = new DockerClientConfiguration(new Uri(DockerApiUri())).CreateClient();
        
        // Создаем зависимости
        PostgresContainer = new PostgresContainer(dockerClient);
        RabbitmqContainer = new RabbitmqContainer(dockerClient);
        MountebankContainer = new MountebankContainer(dockerClient);
        
        // Готовим окружение - удаляем лишнее и запускаем нужные контейнеры
        await RemoveAllContainers(dockerClient);
        await Task.WhenAll(PostgresContainer.StartContainer(), RabbitmqContainer.StartContainer(), MountebankContainer.StartContainer());

        // Стартуем сервер с приложением
        TestServer = CreateServer();
    }

    /// <summary>
    /// Старт сервера с приложением
    /// </summary>
    private static TestServer CreateServer()
    {
        // Конфигурируем наше тестовое прилоежние через переменные окружения
        Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
        
        Environment.SetEnvironmentVariable("DBSETTINGS__USER", "faq");
        Environment.SetEnvironmentVariable("DBSETTINGS__PASSWORD", "faq");
        Environment.SetEnvironmentVariable("DBSETTINGS__HOST", "127.0.0.1");
        Environment.SetEnvironmentVariable("DBSETTINGS__PORT", "5432");
        Environment.SetEnvironmentVariable("DBSETTINGS__DBNAME", "faq");
        
        Environment.SetEnvironmentVariable("AUTHENTICATION_AUTHORITY", "http://localhost:4501/auth/realms/myrealm");

        return new WebApplicationFactory<Program>().Server;
    }

    /// <summary>
    /// Получение Url API Docker
    /// </summary>
    /// <returns>Url</returns>
    /// <exception cref="Exception">Ошибка если не удалось определить ОС</exception>
    private static string DockerApiUri()
    {
        var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        if (isWindows)
        {
            return "npipe://./pipe/docker_engine";
        }

        var isLinux = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
        if (isLinux)
        {
            return "tcp://127.0.0.1:2375";
        }
            
        var isOsx = RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
        if (isOsx)
        {
            return "unix:///var/run/docker.sock";
        }

        throw new Exception("Was unable to determine what OS this is running on");
    }
    
    /// <summary>
    /// Остановить и удалить все контейнеры
    /// </summary>
    private static async Task RemoveAllContainers(DockerClient dockerClient)
    {
        IList<ContainerListResponse> containers = await dockerClient.Containers.ListContainersAsync(new ContainersListParameters());
        foreach (var container in containers)
        {
            await dockerClient.Containers.KillContainerAsync(container.ID, new ContainerKillParameters());
            await dockerClient.Containers.RemoveContainerAsync(container.ID, new ContainerRemoveParameters());
        }
    }
}

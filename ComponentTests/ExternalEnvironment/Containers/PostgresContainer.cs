using System.Data;
using Docker.DotNet;
using Docker.DotNet.Models;
using Npgsql;

namespace ComponentTests.ExternalEnvironment.Containers;

/// <summary>
/// Postgres container
/// </summary>
public class PostgresContainer : BaseContainer
{
    /// <summary>
    /// Конструктор
    /// </summary>
    public PostgresContainer(DockerClient dockerClient) : base("postgres", "12-alpine", dockerClient)
    {
    }

    /// <inheritdoc />
    public override async Task StartContainer()
    {
        await PullImage(Image,Tag);
        
        var exposedPorts = new Dictionary<string, EmptyStruct>
        {
            {
                "5432", default(EmptyStruct)
            }
        };

        var portBindings = new Dictionary<string, IList<PortBinding>>
        {
            {"5432", new List<PortBinding> {new() {HostPort = "5432"}}}
        };
        
        var env = new List<string>
        {
            "POSTGRES_PASSWORD=faq",
            "POSTGRES_USER=faq"
        };
            
        var container = await DockerClient.Containers.CreateContainerAsync(new CreateContainerParameters
        {
            Image = ImageFull,
            Env = env,
            ExposedPorts = exposedPorts,
            HostConfig = new HostConfig
            {
                PortBindings = portBindings,
                PublishAllPorts = true
            }
        });
        
        ContainerId = container.ID;
        await DockerClient.Containers.StartContainerAsync(ContainerId, null);
        await WaitContainer();
    }

    /// <inheritdoc />
    protected override async Task WaitContainer()
    {
        for (var i = 0; i < 30; i++)
        {
            try
            {
                var connectionString = GetConnectionString(null);
                await using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    await connection.CloseAsync();
                    return;
                }
            }
            catch
            {
                //ignore
            }
            
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
    }

    /// <summary>
    /// Удалить данные из БД
    /// </summary>
    public async Task DeleteAllData()
    {
        var connectionString = GetConnectionString("faq");
        await using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
        connection.Open();
        if (connection.State == ConnectionState.Open)
        {
            var clearData = new NpgsqlCommand(
                @"TRUNCATE public.""Categories"" CASCADE;", connection);
            await clearData.ExecuteNonQueryAsync();
            await connection.CloseAsync();
        }
    }

    /// <summary>
    /// Получить строку соединения с БД
    /// </summary>
    /// <returns>ConnectionString</returns>
    private static string GetConnectionString(string? dbName)
    {
        var connectionStringBuilder = new NpgsqlConnectionStringBuilder
        {
            Host = "localhost",
            Port = 5432,
            Database =  dbName,
            Username = "faq",
            Password = "faq"
        };

        return connectionStringBuilder.ConnectionString;
    }
}
using Docker.DotNet;
using Docker.DotNet.Models;

namespace ComponentTests.ExternalEnvironment.Containers;

/// <summary>
/// Rabbitmq Container
/// </summary>
public class RabbitmqContainer : BaseContainer
{
    /// <summary>
    /// Конструктор
    /// </summary>
    public RabbitmqContainer(DockerClient dockerClient) : base("rabbitmq", "3-management-alpine", dockerClient)
    {
    }

    /// <inheritdoc />
    public override async Task StartContainer()
    {
        await PullImage(Image,Tag);
            
        var exposedPorts = new Dictionary<string, EmptyStruct>
        {
            {
                "5672", default
            },
            {
                "15672", default
            },
        };

        var portBindings = new Dictionary<string, IList<PortBinding>>
        {
            {"5672", new List<PortBinding> {new() {HostPort = "5672"}}},
            {"15672", new List<PortBinding> {new() {HostPort = "15672"}}}
        };

        var env = new List<string>
        {
            "RABBITMQ_DEFAULT_USER=guest",
            "RABBITMQ_DEFAULT_PASS=guest",
        };
            
        var container = await DockerClient.Containers.CreateContainerAsync(new CreateContainerParameters
        {
            Image = ImageFull,
            Env = env,
            ExposedPorts = exposedPorts,
            HostConfig = new HostConfig
            {
                PortBindings = portBindings,
                PublishAllPorts = true
            },
        });
            
        ContainerId = container.ID;
        await DockerClient.Containers.StartContainerAsync(ContainerId, null);
        await WaitContainer();
    }

    /// <inheritdoc />
    protected override async Task WaitContainer()
    {
        for (int i = 0; i < 30; i++)
        {
            try
            {
                using var client = new HttpClient();
                var response = await client.GetAsync($"http://localhost:15672");
                if (response.IsSuccessStatusCode)
                {
                    return;
                }
            }
            catch (Exception)
            {
                //ignore
            }
                
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
    }
}
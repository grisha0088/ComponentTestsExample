using System.Net;

namespace ComponentTests.Helpers.Clients.RabbitMq;

/// <summary>
/// Клиент для REST API RabbitMQ. Позволяет работать с реббитом по pull модели - что удобно для тестов
/// </summary>
public class RabbitMqClient
{
    private readonly HttpClient _httpClient;
    const string Vhost = "%2F";

    public RabbitMqClient()
    {
        var httpClientHandler = new HttpClientHandler { Credentials = new NetworkCredential("guest", "guest") };
        _httpClient = new HttpClient(httpClientHandler);
    }
        
    /// <summary>
    /// Получить сообщение из очереди
    /// </summary>
    public async Task<ModelMessageRabbit?> GetMessageFromQueue(string queue)
    {
        var content = new StringContent("{\"count\":1,\"ackmode\":\"ack_requeue_false\",\"encoding\":\"auto\",\"truncate\":50000}");
        var httpResponseMessage = await _httpClient.PostAsync($"http://localhost:15672/api/queues/{Vhost}/{queue}/get", content);

        var payload = httpResponseMessage.Content.ReadAs<ModelMessageRabbit[]>();
        return payload!.FirstOrDefault();
    }
        
    /// <summary>
    /// Очистить очередь
    /// </summary>
    public async Task ClearQueue(string queue) =>
        await _httpClient.DeleteAsync($"http://localhost:15672/api/queues/{Vhost}/{queue}/contents");
}
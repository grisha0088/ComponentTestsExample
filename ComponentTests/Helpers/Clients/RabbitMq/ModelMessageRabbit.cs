namespace ComponentTests.Helpers.Clients.RabbitMq;

/// <summary>
/// Модель ответа от реббит
/// </summary>
public class ModelMessageRabbit
{
    /// <summary>
    /// Полезная нагрузка
    /// </summary>
    public string? Payload { get; set; }
}
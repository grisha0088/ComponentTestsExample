using System.Net;
using ComponentTests.Helpers;
using Faq.Dtos;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.Category.Steps;

[Binding]
public class ThenCategoryStepDefinitions
{
    [Then(@"категория успешно создана")]
    public void CategorySuccessfullyCreated()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
        
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
            
        // десериализуем и сохраним полученную в ответе категорию
        Common.Category = Common.HttpResponseMessage.Content.ReadAs<CategoryDto>();
    }
        
    [Then(@"название категории ""(.*)""")]
    public void CategoryNameShouldBe(string name)
    {
        // название категории должно быть {name}
        Common.Category!.Name.Should().Be(name);
    }
        
    [Then(@"категория успешно обновлена")]
    public void CategorySuccessfullyUpdated()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
                
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
            
        // десериализуем и сохраним полученную в ответе категорию
        Common.Category = Common.HttpResponseMessage.Content.ReadAs<CategoryDto>();
    }        
        
    [Then(@"категория успешно удалена")]
    public void CategorySuccessfullyDeleted()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
        
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
    }
        
    [Then(@"список категорий получен c количеством элементов (.*)")]
    public void ClientGotCategoryListWithCount(int count)
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
        
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
            
        // десериализуем полученный список категорий
        var result = Common.HttpResponseMessage.Content.ReadAs<List<CategoryDto>>();
           
        // количество категорий в списке должно быть {count}
        result!.Count.Should().Be(count);
    }
}
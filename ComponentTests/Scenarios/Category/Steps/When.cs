using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using ComponentTests.ExternalEnvironment;
using Faq.Dtos;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.Category.Steps;

[Binding]
public class WhenCategoryStepDefinitions
{
    [When(@"администратор создаёт категорию с названием ""(.*)""")]
    public async Task ClientCreateCategoryWithName(string name)
    {
        // создаём объект представляющий DTO категории
        var category = new CategoryDto
        (
            Guid.NewGuid(),
            name
        );

        // сериализуем в json этот объект
        var body = JsonSerializer.Serialize(category);
            
        // создаём объект представляющий http запрос
        var request = new HttpRequestMessage(HttpMethod.Post, "/api/v1/categories")
        {
            // задаём контент запроса
            Content = new StringContent(body, Encoding.UTF8, "application/json"),
        };
           
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
        
    [When(@"администратор обновляет название категории на ""(.*)""")]
    public async Task ClientUpdateCategoryNameTo(string name)
    {
        // создаём объект представляющий DTO категории
        var category = new CategoryDto
        (
            Guid.NewGuid(),
            name
        );

        // сериализуем в json этот объект
        var body = JsonSerializer.Serialize(category);
            
        // создаём объект представляющий http запрос
        var request = new HttpRequestMessage(HttpMethod.Put, $"/api/v1/categories/{Common.Category!.Id}")
        {
            // задаём контент запроса
            Content = new StringContent(body, Encoding.UTF8, "application/json")
        };
            
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
        
    [When("администратор запрашивает список всех категорий")]
    public async Task ClientRequestAllCategories()
    {
        // создаём объект представляющий http запрос на получение всех категрий
        var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/categories");
        
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
        
    [When(@"администратор удаляет категорию")]
    public async Task ClientDeleteCategory()
    {
        // создаём объект представляющий http запрос на получение категории по id
        var request = new HttpRequestMessage(HttpMethod.Delete, $"/api/v1/categories/{Common.Category!.Id}");
        
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
}
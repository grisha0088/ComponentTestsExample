#noinspection SpellCheckingInspection,CucumberUndefinedStep,NonAsciiCharacters
Feature: Category
  Background:
    Given кейклоак работает и настроен
    When клиент является администратором приложения
  
  Scenario: Администратор может создать/отредактировать/получить список категорий/удалить категории
    When администратор создаёт категорию с названием "SomeCategoryName"
    Then категория успешно создана
    And название категории "SomeCategoryName"
    When администратор обновляет название категории на "SomeCategoryNameUpdated"
    Then категория успешно обновлена
    And название категории "SomeCategoryNameUpdated"
    When администратор запрашивает список всех категорий
    Then список категорий получен c количеством элементов 1    
    When администратор удаляет категорию
    Then категория успешно удалена
    When администратор запрашивает список всех категорий
    Then список категорий получен c количеством элементов 0
using ComponentTests.Helpers;
using ComponentTests.Helpers.JwtAuthorisation.Models;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.CommonSteps;

[Binding]
public class WhenCommonStepDefinitions
{
    [When("клиент является администратором приложения")]
    public async Task ClientIsAdmin()
    {
        // создаём http клиент для отпавки запроса на получение токена
        using var httpClient = new HttpClient();
        
        // задаём базовый адрес кейклоака
        httpClient.BaseAddress = new Uri($"http://localhost:4501");
        
        // создаём запрос на получение токена
        var request = new HttpRequestMessage(HttpMethod.Post, $"/auth/realms/myrealm/protocol/openid-connect/token");
        
        // отправляем запрос
        var response = await httpClient.SendAsync(request);
        
        // десериализуем полученный ответ и прихраниваем AccessToken для дальнейшего использования в запросах к сервису
        Common.AuthToken = response.Content.ReadAs<Token>()!.AccessToken;
    }
}
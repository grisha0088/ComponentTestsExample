﻿using System.Net;
using ComponentTests.ExternalEnvironment;
using ComponentTests.Helpers.JwtAuthorisation;
using MbDotNet.Models;
using MbDotNet.Models.Imposters;
using MbDotNet.Models.Stubs;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.CommonSteps;

[Binding]
public class GivenCommonStepDefinitions
{
    [Given("кейклоак работает и настроен")]
    public async Task KeyCloakIsWorking()
    {
        // удалим импостер, если он уже есть
        await ExtEnvironment.MountebankClient.DeleteImposterAsync(4501);
        
        // созданим импостер кейклоака
        await ExtEnvironment.MountebankClient.CreateHttpImposterAsync(new HttpImposter(4501, "keyCloak",
            new HttpImposterOptions()
            {
                // разрешим cors, чтобы можно было удобно получить токен из UI сваггера
                AllowCORS = true
            }));

        // мокаем запрос на получение настроек для проверки токена
        await ExtEnvironment.MountebankClient.AddHttpImposterStubAsync(4501,
            new HttpStub()
                .OnPathAndMethodEqual($"/auth/realms/myrealm/.well-known/openid-configuration", Method.Get)
                .ReturnsJson(HttpStatusCode.OK, KeyCloakResponseGenerator.GetOpenidConfiguration("myrealm")), 0);

        // мокаем запрос на получение сертификата для проверки токена
        await ExtEnvironment.MountebankClient.AddHttpImposterStubAsync(4501,
            new HttpStub()
                .OnPathAndMethodEqual($"/auth/realms/myrealm/protocol/openid-connect/certs", Method.Get)
                .ReturnsJson(HttpStatusCode.OK, KeyCloakResponseGenerator.GetCertificates()), 1);
        
        // мокаем запрос на получение токена 
        await ExtEnvironment.MountebankClient.AddHttpImposterStubAsync(4501,
            new HttpStub()
                .OnPathAndMethodEqual($"/auth/realms/myrealm/protocol/openid-connect/token", Method.Post)
                .ReturnsJson(HttpStatusCode.OK, KeyCloakResponseGenerator.GetToken("myrealm", new Dictionary<string, string>())));
    }
}
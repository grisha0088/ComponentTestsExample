#noinspection SpellCheckingInspection,CucumberUndefinedStep,NonAsciiCharacters
Feature: Question
  Background:
    Given кейклоак работает и настроен
    When клиент является администратором приложения
    When администратор создаёт категорию с названием "SomeCategoryName"
    Then категория успешно создана
  
  Scenario: Администратор может создать/отредактировать/получить список вопросов/удалить вопрос
    When администратор создаёт вопрос с названием "SomeQuestionName" и телом "SomeQuestionBody"
    Then вопрос успешно создан
    And название вопроса "SomeQuestionName"
    And тело вопроса "SomeQuestionBody"
    When администратор обновляет название вопроса на "SomeQuestionNameUpdated" и тело на "SomeQuestionBodyUpdated"
    Then вопрос успешно обновлен
    And название вопроса "SomeQuestionNameUpdated"
    And тело вопроса "SomeQuestionBodyUpdated"
    When администратор запрашивает список всех вопросов
    Then список вопросов получен c количеством элементов 1    
    When администратор удаляет вопрос
    Then вопрос успешно удален
    When администратор запрашивает список всех вопросов
    Then список вопросов получен c количеством элементов 0
    
  Scenario Outline: Поиск вопросов работает верно
    When администратор создаёт вопросы
      | Name   | Answer    |
      | First  | AbcXXX    |
      | Second | XXXcda    |
      | Third  | SecondYYY |
    When администратор выполняет поиск вопросов "<SearchQuery>"
    Then список вопросов получен c количеством элементов <Count>
  Examples: 
    | SearchQuery | Count |
    | First       | 1     |
    | Fir         | 1     |
    | ZZZ         | 0     |
    | first       | 1     |
    | XXX         | 2     |
    | Second      | 2     |
    
  Scenario: Вопрос корректно отправляется в rabbitMq
    When администратор создаёт вопрос с названием "SomeQuestionName" и телом "SomeQuestionBody"
    Then вопрос успешно создан
    And вопрос успешно отправлен
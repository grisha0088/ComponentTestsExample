using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using ComponentTests.ExternalEnvironment;
using Faq.Dtos;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.Question.Steps;

[Binding]
public class WhenQuestionStepDefinitions
{
    [When(@"администратор создаёт вопрос с названием ""(.*)"" и телом ""(.*)""")]
    public async Task AdministratorCreateQuestionWithNameAndBody(string name, string answer)
    {
        // создаём объёкт представляющий DTO вопроса
        var question = new QuestionDto
        (
            Guid.NewGuid(),
            name,
            answer,
            Common.Category!.Id
        );

        // сериализуем вопрос в json
        var body = JsonSerializer.Serialize(question);
            
        // создаём объект представляющий запрос на создание вопроса
        var request = new HttpRequestMessage(HttpMethod.Post, "/api/v1/questions")
        {
            // зададим контент запроса
            Content = new StringContent(body, Encoding.UTF8, "application/json")
        };
            
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
        
    [When(@"администратор обновляет название вопроса на ""(.*)"" и тело на ""(.*)""")]
    public async Task AdministratorUpdateQuestionWithNameAndBody(string name, string answer)
    {
        // создаём объёкт представляющий DTO вопроса
        var question = new QuestionDto
        (
            Guid.NewGuid(),
            name,
            answer,
            Common.Category!.Id
        );

        // сериализуем вопрос в json
        var body = JsonSerializer.Serialize(question);
            
        // создаём объект представляющий запрос на обновление вопроса
        var request = new HttpRequestMessage(HttpMethod.Put, $"/api/v1/questions/{Common.Question!.Id}")
        {
            // зададим контент запроса
            Content = new StringContent(body, Encoding.UTF8, "application/json")
        };
            
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
            
    [When("администратор удаляет вопрос")]
    public async Task AdministratorDeleteQuestion()
    {
        // создаём объект представляющий http запрос на удаление последнего вопроса 
        var request = new HttpRequestMessage(HttpMethod.Delete, $"/api/v1/questions/{Common.Question!.Id}");
        
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
        
    [When("администратор запрашивает список всех вопросов")]
    public async Task AdministratorRequestAllQuestions()
    {
        // создаём объект представляющий http запрос на получение всех вопросов
        var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/questions");
        
        // устанавливаем хэдер авторизации
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        
        // отправляем запрос на сервер и сохраняем его результат
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
    
    [When(@"администратор выполняет поиск вопросов ""(.*)""")]
    public async Task AdministratorSeqrchQuestions(string search)
    {
        var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/questions?search={search}");
        request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
        Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
    }
    
    [When("администратор создаёт вопросы")]
    public async Task AdministratorCreateQuestions(Table table)
    {
        // переберём в цикле все переданные значения с данными вопросов, которые требуется создать
        foreach (var row in table.Rows)
        {
            // создаём объёкт представляющий DTO вопроса
            var question = new QuestionDto
            (
                Guid.NewGuid(),
                row[0],
                row[1],
                Common.Category!.Id
            );

            // сериализуем вопрос в json
            var body = JsonSerializer.Serialize(question);
            
            // создаём объект представляющий запрос на создание вопроса
            var request = new HttpRequestMessage(HttpMethod.Post, "/api/v1/questions")
            {
                Content = new StringContent(body, Encoding.UTF8, "application/json")
            };
            
            // устанавливаем хэдер авторизации
            request.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer " + Common.AuthToken);
            
            // отправляем запрос на сервер и сохраняем его результат
            Common.HttpResponseMessage = await ExtEnvironment.TestServer.CreateClient().SendAsync(request);
        }
    }
}
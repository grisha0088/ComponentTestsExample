using System.Net;
using ComponentTests.ExternalEnvironment;
using ComponentTests.Helpers;
using Faq.Dtos;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace ComponentTests.Scenarios.Question.Steps;

[Binding]
public class ThenQuestionStepDefinitions
{
    [Then(@"вопрос успешно создан")]
    public void QuestionSuccessfullyCreated()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
                
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
        
        // десериализуем и сохраним полученный в ответе вопрос
        Common.Question = Common.HttpResponseMessage.Content.ReadAs<QuestionDto>();
    }    
    
    [Then(@"вопрос успешно отправлен")]
    public async Task QuestionSuccessfullySent()
    {
        // прочитаем последнее сообщение из RabbitMQ, которое должно содержать отправленный нами вопрос
        var messagesFromQueue = await ExtEnvironment.RabbitMqClient.GetMessageFromQueue("FaqQueue");
        
        // проверим, что сообщение было отправлено
        messagesFromQueue.Should().NotBeNull();

        // десериализуем вопрос из сообщения из RabbitMQ
        var question = messagesFromQueue!.Payload!.ReadAs<QuestionDto>();
        
        // проверим, что поля вопроса выгружены верно
        question.Should().NotBeNull();
        question!.Id.Should().Be(Common.Question!.Id);
        question.Title.Should().Be(Common.Question.Title);
        question.Answer.Should().Be(Common.Question.Answer);
        question.CategoryId.Should().Be(Common.Question.CategoryId);
    }
        
    [Then(@"название вопроса ""(.*)""")]
    public void QuestionNameShouldBe(string name)
    {
        // заголовок вопроса должен быть {name}
        Common.Question!.Title.Should().Be(name);
    }   
    
    [Then(@"тело вопроса ""(.*)""")]
    public void QuestionAnswerShouldBe(string answer)
    {
        // ответ на вопрос должен быть {answer}
        Common.Question!.Answer.Should().Be(answer);
    }
        
    [Then(@"вопрос успешно обновлен")]
    public void QuestionSuccessfullyUpdated()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
                
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
        
        // десериализуем и сохраним полученный в ответе вопрос
        Common.Question = Common.HttpResponseMessage.Content.ReadAs<QuestionDto>();
    }        
        
    [Then(@"вопрос успешно удален")]
    public void CategorySuccessfullyDeleted()
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
                
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
    }
        
    [Then(@"список вопросов получен c количеством элементов (.*)")]
    public void ClientGotQuestionListWithCount(int count)
    {
        // проверим, что ответ на запрос не пустой
        Common.HttpResponseMessage.Should().NotBeNull();
                
        // проверим, что код ответа 200
        Common.HttpResponseMessage!.StatusCode.Should().Be(HttpStatusCode.OK);
          
        // десериализуем полученный в ответе список вопросов
        var result = Common.HttpResponseMessage.Content.ReadAs<List<QuestionDto>>();
           
        // количество вопросов должно быть {count}
        result!.Count.Should().Be(count);
    }
}
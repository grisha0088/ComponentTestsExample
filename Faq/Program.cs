using Faq.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.ConfigureSettings(builder.Configuration);
builder.Services.AddControllers();
builder.Services.ConfigureDatabase();
builder.Services.AddSwaggerGen(c =>
    {
        var authority = builder.Configuration.GetValue<string>("AUTHENTICATION_AUTHORITY");
        c.AddSecurityDefinition("myAuth", new OpenApiSecurityScheme
        {
            Type = SecuritySchemeType.OAuth2,
            Flows = new OpenApiOAuthFlows
            {
                Password = new OpenApiOAuthFlow
                {
                    AuthorizationUrl = new Uri($"{authority}/protocol/openid-connect/auth"),
                    TokenUrl = new Uri($"{authority}/protocol/openid-connect/token"),
                    Scopes = new Dictionary<string, string>()
                }
            }
        });
        
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                        { Type = ReferenceType.SecurityScheme, Id = "myAuth"}
                },
                new List<string>()
            }
        });
    }
    );

builder.Services.AddAuthentication().AddJwtBearer(options =>
{
    var authority = builder.Configuration.GetValue<string>("AUTHENTICATION_AUTHORITY");
    options.Authority = authority;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateAudience = false,
        ClockSkew = TimeSpan.Zero,
    };
    
    if (builder.Environment.IsDevelopment())
    {
        options.RequireHttpsMetadata = false;
    }
});

builder.Services.AddAuthorization(options =>
{
    var myrealmPolicyBuilder = new AuthorizationPolicyBuilder()
        .RequireAuthenticatedUser()
        .AddAuthenticationSchemes("myrealm");

    options.AddPolicy("myrealm", myrealmPolicyBuilder.Build());
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();
app.MigrateDatabase();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();
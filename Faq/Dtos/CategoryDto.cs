using System.Text.Json.Serialization;
using Faq.Infrastructure.Models;

namespace Faq.Dtos;

public class CategoryDto
{
    [JsonConstructor]
    public CategoryDto(Guid id, string name)
    {
        Id = id;
        Name = name;
    }

    public CategoryDto(Category category)
    {
        Id = category.Id;
        Name = category.Name;
    }
    
    public Guid Id { get; set; }
    public string Name { get; set; }
}
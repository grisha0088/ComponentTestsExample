using System.Text.Json.Serialization;
using Faq.Infrastructure.Models;

namespace Faq.Dtos;

public class QuestionDto
{
    [JsonConstructor]
    public QuestionDto(Guid id, string title, string answer, Guid categoryId)
    {
        Id = id;
        Title = title;
        Answer = answer;
        CategoryId = categoryId;
    }

    public QuestionDto(Question question)
    {
        Id = question.Id;
        Title = question.Title;
        Answer = question.Answer;
        CategoryId = question.CategoryId;
    }
    
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Answer { get; set; }
    public Guid CategoryId { get; set; }
}
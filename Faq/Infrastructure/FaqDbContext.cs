using Faq.Infrastructure.Configuration;
using Faq.Infrastructure.Models;
using FaqInfrastructure.ModelConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Faq.Infrastructure;

// для выполнения миграций нужно выполнить 
//export DBSETTINGS__USER=faq
//export DBSETTINGS__PASSWORD=faq
//export DBSETTINGS__HOST=127.0.0.1
//export DBSETTINGS__PORT=5432
//export DBSETTINGS__DBNAME=faq
public class FaqDbContext : DbContext
{
    public DbSet<Category> Categories { get; set; }
    public DbSet<Question> Questions { get; set; }

    public FaqDbContext(DbContextOptions<FaqDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new CategoryConfiguration());
        modelBuilder.ApplyConfiguration(new QuestionConfiguration());
    }
}
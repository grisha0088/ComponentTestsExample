namespace Faq.Infrastructure.Models;

public class Question
{
    public Question(Guid id, string title, string answer, Guid categoryId)
    {
        Id = id;
        Title = title;
        Answer = answer;
        CategoryId = categoryId;
    }

    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Answer { get; set; }
    public Guid CategoryId { get; set; }
    public Category Category { get; set; } = null!;
}
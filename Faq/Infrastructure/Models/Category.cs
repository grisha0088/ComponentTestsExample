namespace Faq.Infrastructure.Models;

public class Category
{
    public Category(Guid id, string name)
    {
        Id = id;
        Name = name;
        Questions = new List<Question>();
    }

    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<Question> Questions { get; set; }
}
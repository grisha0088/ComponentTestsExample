using Faq.Configuration.Models;
using Faq.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Npgsql;

namespace Faq.Configuration;

public static class ServiceCollectionExtensions
{
    public static void ConfigureSettings(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<DbSettings>(configuration.GetSection("DBSETTINGS"));
    }

    public static void ConfigureDatabase(this IServiceCollection services)
    {
        services.AddDbContext<FaqDbContext>(((provider, options) =>
            {
                var settings = provider.GetRequiredService<IOptions<DbSettings>>().Value;
                options.UseNpgsql(GetConnectionString(settings));
            }),
            ServiceLifetime.Scoped,
            ServiceLifetime.Singleton);
    }

    private static string GetConnectionString(DbSettings settings)
    {
        var connectionStringBuilder = new NpgsqlConnectionStringBuilder
        {
            Host = settings.Host,
            Port = settings.Port,
            Database = settings.DbName,
            Username = settings.User,
            Password = settings.Password,
            NoResetOnClose = true
        };

        return connectionStringBuilder.ConnectionString;
    }
}
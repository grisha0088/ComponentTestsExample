using Faq.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace Faq.Configuration;

public static class ApplicationBuilderExtensions
{
    public static void MigrateDatabase(this IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();

        var db = scope.ServiceProvider.GetRequiredService<FaqDbContext>().Database;
        
        db.SetCommandTimeout(TimeSpan.FromMinutes(5));
        db.Migrate();

        using var connection = (NpgsqlConnection)db.GetDbConnection();
        connection.Open();
        connection.ReloadTypes();
    }
}
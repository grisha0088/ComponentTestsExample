namespace Faq.Configuration.Models;

public class DbSettings
{
    public required string User { get; set; }
    public required string Password { get; set; }
    public required string Host { get; set; }
    public required int Port { get; set; }
    public required string DbName { get; set; }
}
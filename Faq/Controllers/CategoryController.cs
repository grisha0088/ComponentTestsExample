using Faq.Dtos;
using Faq.Infrastructure;
using Faq.Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Faq.Controllers;

[ApiController]
[Authorize]
[Route("api/v1/categories")]
public class CategoryController : ControllerBase
{
    private readonly FaqDbContext _dbContext;
    
    public CategoryController(FaqDbContext dbContext) => _dbContext = dbContext;

    [HttpGet]
    public async Task<List<CategoryDto>> GetAllCategoriesAsync() 
        => await _dbContext.Categories.Include(x=>x.Questions)
            .Select(x=>new CategoryDto(x)).ToListAsync();

    [HttpGet("{id}")]
    public async Task<CategoryDto> GetCategoryById([FromRoute] Guid id)
    {
        var category = await _dbContext.Categories.Include(x=>x.Questions)
            .FirstAsync(x => x.Id == id);
        
        return new CategoryDto(category);
    }

    [HttpPost]
    public async Task<CategoryDto> CreateCategory([FromBody] CategoryDto categoryDto)
    {
        var category = new Category(categoryDto.Id, categoryDto.Name);
        
        await _dbContext.Categories.AddAsync(category);
        await _dbContext.SaveChangesAsync();
        
        return new CategoryDto(category);
    }

    [HttpPut("{id}")]
    public async Task<CategoryDto> UpdateCategory([FromRoute] Guid id, [FromBody] CategoryDto categoryDto)
    {
        var category = await _dbContext.Categories.FirstAsync(x => x.Id == id);
        category.Name = categoryDto.Name;
        
        await _dbContext.SaveChangesAsync();
        
        return new CategoryDto(category);
    }

    [HttpDelete("{id}")]
    public async Task DeleteCategory([FromRoute] Guid id)
    {
        var category = await _dbContext.Categories.FirstAsync(x => x.Id == id);
        
        _dbContext.Categories.Remove(category);
        await _dbContext.SaveChangesAsync();
    }
}
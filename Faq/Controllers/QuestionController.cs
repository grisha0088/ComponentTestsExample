using System.Text;
using System.Text.Json;
using Faq.Dtos;
using Faq.Infrastructure;
using Faq.Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;

namespace Faq.Controllers;

[ApiController]
[Authorize]
[Route("api/v1/questions")]
public class QuestionController : ControllerBase
{
    private readonly FaqDbContext _dbContext;

    public QuestionController(FaqDbContext dbContext) => _dbContext = dbContext;

    [HttpGet("{id}")]
    public async Task<QuestionDto?> GetQuestionById([FromRoute] Guid id)
    {
        var question = await _dbContext.Questions.FirstAsync(x => x.Id == id);
        return new QuestionDto(question);
    }
    
    [HttpGet]
    public async Task<List<QuestionDto>> Search([FromQuery]string? search)
    {
        var questionQuery = _dbContext.Questions.AsQueryable();
        
        if (search != null)
        {
            questionQuery = questionQuery.Where(x =>
                EF.Functions.ILike(x.Title, $"%{search}%") ||
                EF.Functions.ILike(x.Answer, $"%{search}%")
            );
        }
        var questions = await questionQuery.ToListAsync();
        return questions.Select(x => new QuestionDto(x)).ToList();
    }

    [HttpPost]
    public async Task<QuestionDto> CreateQuestion([FromBody] QuestionDto questionDto)
    {
        var question = new Question(questionDto.Id, questionDto.Title, questionDto.Answer, questionDto.CategoryId);

        _dbContext.Questions.Add(question);
        await _dbContext.SaveChangesAsync();
        
        var factory = new ConnectionFactory
        {
            UserName = "guest",
            Password = "guest",
            VirtualHost = "/",
            HostName = "localhost",
            Port = 5672
        };
        
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.ExchangeDeclare("FaqExchange", ExchangeType.Direct, durable: true);
            channel.QueueDeclare("FaqQueue", true, false, false);
            channel.QueueBind("FaqQueue", "FaqExchange", "FaqRoutingKey");

            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(question));
            
            channel.BasicPublish(exchange:"FaqExchange",
                routingKey:"FaqRoutingKey",
                basicProperties:null,
                body: body);
        }
        
        return new QuestionDto(question);
    }

    [HttpPut("{id}")]
    public async Task<QuestionDto> UpdateQuestion([FromRoute] Guid id, [FromBody] QuestionDto questionDto)
    {
        var question = await _dbContext.Questions.FirstAsync(x => x.Id == id);
        
        question.Title = questionDto.Title;
        question.Answer = questionDto.Answer;
        question.CategoryId = questionDto.CategoryId;
        
        await _dbContext.SaveChangesAsync();
        return new QuestionDto(question);
    }

    [HttpDelete("{id}")]
    public async Task DeleteQuestion([FromRoute] Guid id)
    {
        var question = await _dbContext.Questions.FirstAsync(x => x.Id == id);
        
        _dbContext.Questions.Remove(question);
        await _dbContext.SaveChangesAsync();
    }
}